/**
 * THIS IS A COMPILED TYPESCRIPT FILE
 * SEE THE ACTUAL SOURCE CODE AT User:Lectrician1/discographies.js/ts.js
 * 
 * Name: discographies.js
 * Description: Shows useful discography data and functions on 
 *  discography items
 * Note: Only meant to work on page refresh. This is because the 
 *  lag time between creating a statement like "publication date"
 *  and it showing up in BlazeGraph and then by this script is high.
 * Author: Lectrician1
 * License: CC0
 * Functions taken from: 
 *  https://www.wikidata.org/wiki/User:Nikki/ExMusica.js
 *  https://www.wikidata.org/wiki/User:Magnus_Manske/duplicate_item.js
 */


// Utility functions

var entityHTML = (link: string, label: string) => `<a href="${link}">${label}</a>`

var addItemStatement = (propID, valueID) => ({
    [propID]: [
        {
            "mainsnak": {
                "snaktype": "value",
                "property": propID,
                "datavalue": {
                    "value": {
                        "entity-type": "item",
                        "id": valueID
                    },
                    "type": "wikibase-entityid"
                },
                "datatype": "wikibase-item"
            },
            "type": "statement",
            "rank": "normal"
        }
    ]
})

function entityHasStatement(property: string, values: Array<string>, entityClaims: WikibaseUIClaims,) {
    for (let claim of entityClaims[property]) {
        if (values.includes(claim.mainsnak.datavalue.value.id))
            return true;
    }
    return false;
}

async function getEditToken(callback) {
    const d = await $.get('/w/api.php', {
        action: 'query',
        meta: 'tokens',
        format: 'json'
    })
    var token = d.query.tokens.csrftoken;
    if (typeof token == 'undefined') {
        alert("Problem getting edit token");
        return;
    }
    console.log(d)
    await callback(token);
}

async function createNewItem(q, data) {
    await getEditToken(async function (token) {
        console.log(data)
        const d = await $.post('/w/api.php', {
            action: 'wbeditentity',
            new: 'item',
            data: JSON.stringify(data),
            token: token,
            summary: 'Item release created from ' + q,
            format: 'json'
        })
        if (d.success == 1) {
            var nq = d.entity.id
            var url = "/wiki/" + nq;
            window.open(url, '_blank');
        } else {
            console.log(d);
            alert("A problem occurred, check JavaScript console for errors");
        }
    });
}

async function copyItem(thisEntityPageData: WikibaseUIItem, askLabels: boolean, propertiesToKeep: string[], claimsToAdd: Object) {
    const d = await $.get('/w/api.php', {
        action: 'wbgetentities',
        ids: thisEntityPageData.title,
        format: 'json'
    })
    var eNow = d.entities[thisEntityPageData.title];

    $.each(eNow.claims, function (p, v) {
        if (propertiesToKeep.includes(String(p))) $.each(v, (i, c) => {
            delete c.id
        });
        else delete eNow.claims[p]
    });

    var data = {
        // descriptions : e.descriptions || {} ,
        // labels : e.labels || {} ,
        // aliases : e.aliases || {} ,
        labels: Object,
        aliases: Object,
        claims: {
            ...eNow.claims,
            ...claimsToAdd
        }
    };

    console.log(data)

    if (askLabels && window.confirm("Duplicate all labels and aliases? You will need to fix them in all languages!")) {
        data.labels = eNow.labels || {};
        data.aliases = eNow.aliases || {};
    }
    await createNewItem(thisEntityPageData.title, data);

}

async function sparqlQuery(query: string) {
    return await $.post(siteData.sparqlEndpoint, { query: query })
}

async function sparqlAsk(query: string, callback: () => void) {
    (await sparqlQuery(query) as BoolResponse).boolean ? callback() : {}
}

var entityLinkNameIDHTML = (name: string, id: string) => `<a href="https://www.wikidata.org/wiki/Special:EntityData/${id}">${name}</a>`

// Wikibase API interfaces
interface WikibaseUIItem {
    title: string
    type: string
    claims: WikibaseUIClaims
}

interface WikibaseUIClaims {
    [property: string]: [{
        mainsnak: {
            datavalue: {
                value: {
                    id: string
                }
            }
        }
    }]
}

interface BoolResponse {
    boolean: boolean
    head: {}
}

interface Results {
    [queryVariable: string]: string
}

// Non-parsed query data structure

interface SelectResponse {
    results: {
        bindings: []
    }
}

interface WikibaseEntity {
    id: string
}

abstract class WikibaseProperty implements WikibaseEntity {
    constructor(public id, public label) {
        this.id = id
        this.label = label
    }
}

class WikibaseItemProperty extends WikibaseProperty { }

class WikibaseDateProperty extends WikibaseProperty { }

class WikibaseQuantityProperty extends WikibaseProperty { }

class WikibaseMonolingualTextProperty extends WikibaseProperty { }

interface SiteData {
    sparqlEndpoint: string

    // Store entity ids used by the script in a labeled object so that they are easily understood in the codebase
    entities: {
        items: {
            release_group: WikibaseEntity
            release: WikibaseEntity
            various_artists: WikibaseEntity
            musical_work_composition: WikibaseEntity
            song: WikibaseEntity
        }
        properties: {
            instance_of: WikibaseItemProperty
            subclass_of: WikibaseItemProperty
            title: WikibaseMonolingualTextProperty
            genre: WikibaseItemProperty
            performer: WikibaseItemProperty
            record_label: WikibaseItemProperty
            publication_date: WikibaseDateProperty
            number_of_parts_of_this_work: WikibaseEntity
            tracklist: WikibaseEntity
            release_of: WikibaseItemProperty
            form_of_creative_work: WikibaseItemProperty
            language_of_work_or_name: WikibaseItemProperty
        }
    }
}


abstract class QueryVariable {
    name: string
    variables: string[]
    constructor(name) {
        this.name = name
    }

    abstract createGroupQuery()

    abstract createSelectQuery()

    abstract resultToTableCell(result: object)
}

abstract class PropertyQueryVariable extends QueryVariable {
    property: WikibaseProperty
    optional: boolean
    constructor(name, property: WikibaseProperty, optional: boolean) {
        super(name)
        this.property = property
        this.optional = optional
    }

    abstract createGroupQuery(): string

    abstract createSelectQuery()

    abstract createQuery()
}

class ResultQueryVariable extends QueryVariable {
    constructor(name) {
        super(name)
    }
    createGroupQuery() {
        return `?${this.name} ?${this.name}Label `
    }

    createSelectQuery() {
        return `?${this.name} ?${this.name}Label `
    }

    resultToTableCell(result: object) {
        return entityHTML(result[this.name], result[`${this.name}Label`])
    }
}

class ItemQueryVariable extends PropertyQueryVariable {
    constructor(name, property: WikibaseItemProperty, optional: boolean) {
        super(name, property, optional)
    }

    createGroupQuery() {
        return `(GROUP_CONCAT (DISTINCT ?${this.name}; SEPARATOR = "|") AS ?${this.name}s)
(GROUP_CONCAT (DISTINCT ?${this.name}Label; SEPARATOR = "|") AS ?${this.name}Labels)
`
    }

    createSelectQuery() {
        return `?${this.name} ?${this.name}Label `
    }

    createQuery() {
        return `?${parentVariable} wdt:${this.property.id} ?${this.name}.
`
    }

    resultToTableCell(result: object) {
        let links = result[`${this.name}s`]
        let labels = result[`${this.name}Labels`]

        if (links.value === '') return ''

        const splitLinks = links.value.split('|')
        const splitLabels = labels.value.split('|')
        return splitLinks.map((link, index) => entityHTML(link, splitLabels[index])).join(', ')
    }
}

class DateQueryVariable extends PropertyQueryVariable {
    constructor(name, property: WikibaseDateProperty, optional: boolean) {
        super(name, property, optional)
    }

    createGroupQuery() {
        return `(GROUP_CONCAT (DISTINCT ?${this.name}; SEPARATOR = "|") AS ?${this.name}s)
`
    }

    createSelectQuery() {
        return `?${this.name} `
    }

    createQuery() {
        return `?${parentVariable} wdt:${this.property.id} ?${this.name}.
`
    }

    resultToTableCell(result: object) {
        const dates = result[`${this.name}s`]
        if (dates.value === '') return ''
        const splitDates = dates.value.split('|')
        const prettyDates = splitDates.map((date) => new Date(date).toISOString().split('T')[0])
        return prettyDates.join(', ')
    }
}

class MonolingualTextQueryVariable extends PropertyQueryVariable {
    constructor(name, property: WikibaseMonolingualTextProperty, optional: boolean) {
        super(name, property, optional)
    }

    createGroupQuery() {
        return `(GROUP_CONCAT (DISTINCT ?${this.name}; SEPARATOR = "|") AS ?${this.name}s)
(GROUP_CONCAT (DISTINCT ?${this.name}Language; SEPARATOR = "|") AS ?${this.name}Languages)
`
    }

    createSelectQuery() {
        return `?${this.name} ?${this.name}Language `
    }

    createQuery() {
        return `?${parentVariable.name} wdt:${this.property.id} ?${this.name}.
BIND ( lang(?${this.name}) AS ?${this.name}Language )
`
    }

    resultToTableCell(result: object) {
        let titles = result[`${this.name}s`]
        let languages = result[`${this.name}Languages`]

        if (titles.value === '') return ''
        const splitTitles = titles.value.split('|')
        const splitLanguages = languages ? languages.value.split('|') : []
        const titlesWithLang = splitTitles.map((title, index) => `${title} (${splitLanguages[index]})`)
        return titlesWithLang.join(', ')
    }
}

let parentVariable = new ResultQueryVariable('release')

var siteData: SiteData

switch (window.location.hostname) {
    case 'www.wikidata.org':
        siteData = {
            sparqlEndpoint: 'https://query.wikidata.org/sparql?format=json',
            entities: {
                items: {
                    release_group: {
                        id: 'Q108346082'
                    },
                    release: {
                        id: 'Q2031291'
                    },
                    various_artists: {
                        id: 'Q3108914'
                    },
                    musical_work_composition: {
                        id: 'Q105543609'
                    },
                    song: {
                        id: 'Q7366'
                    }
                },
                properties: {
                    instance_of: new WikibaseItemProperty('P31', 'instance_of'),
                    subclass_of: new WikibaseItemProperty('P279', 'subclass_of'),
                    title: new WikibaseMonolingualTextProperty('P1476', 'title'),
                    genre: new WikibaseItemProperty('P136', 'genre'),
                    performer: new WikibaseItemProperty('P175', 'performer'),
                    record_label: new WikibaseItemProperty('P264', 'record_label'),
                    publication_date: new WikibaseDateProperty('P577', 'publication_date'),
                    number_of_parts_of_this_work: new WikibaseQuantityProperty('P2635', 'number_of_parts_of_this_work'),
                    tracklist: new WikibaseItemProperty('P658', 'tracklist'),
                    release_of: new WikibaseItemProperty('P9831', 'release_of'),
                    form_of_creative_work: new WikibaseItemProperty('P7937', 'form_of_creative_work'),
                    language_of_work_or_name: new WikibaseItemProperty('P407', 'language_of_work_or_name')
                }
            }
        }
        break
}

var siteEntities = siteData!.entities

const instanceOfClass = `wdt:${siteEntities.properties.instance_of.id}/wdt:${siteEntities.properties.subclass_of.id}*`

const api = new mw.Api

mw.hook("wikibase.entityPage.entityLoaded").add(async function (thisEntityPageData: WikibaseUIItem) {

    // Script run conditions
    if (thisEntityPageData.type !== "item")
        return;

    if (!thisEntityPageData.claims.hasOwnProperty(siteEntities.properties.instance_of.id))
        return;

    const ifReleaseGroupQuery = `ASK {
        wd:${thisEntityPageData.title} ${instanceOfClass} wd:${siteEntities.items.release_group};
        wdt:${siteEntities.properties.performer} [].
    }`;

    // Run 
    (await sparqlQuery(ifReleaseGroupQuery) as BoolResponse).boolean ? releaseGroupFeature(thisEntityPageData) : {}
})

function releaseGroupFeature(thisEntityPageData: WikibaseUIItem) {
    createReleaseFeature(thisEntityPageData)
    chronologicalDataFeature(thisEntityPageData)
}

interface SelectResult {
    value: string,
}

interface ReleaseGroupRelease {
    release: SelectResult
}

function createReleaseFeature(thisEntityPageData: WikibaseUIItem) {
    $('body').append(`<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">
  
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>`)

    // Create release button
    $('#toc').after(`<div><a id="createRelease">Create a release for this release group</a></div>`)

    $('#createRelease').on('click', async function () {

        // Get release item of this release group
        const releaseGroupReleaseQuery = `SELECT ?release WHERE {
                    ?release wdt:${siteEntities.properties.release_of} wd:${thisEntityPageData.claims.P31[0].mainsnak.datavalue.value.id};
                             wdt:${siteEntities.properties.subclass_of}* wd:${siteEntities.items.release}.
                  }`

        var releaseGroupReleaseResponse = await sparqlQuery(releaseGroupReleaseQuery) as SelectResponse

        var propertiesToKeep = [
            siteEntities.properties.performer,
            siteEntities.properties.genre,
            siteEntities.properties.number_of_parts_of_this_work,
            siteEntities.properties.publication_date,
            siteEntities.properties.record_label,
            siteEntities.properties.title,
            siteEntities.properties.tracklist
        ].map(property => property.id)

        var releaseTypeID = linkToID((releaseGroupReleaseResponse.results.bindings as Array<ReleaseGroupRelease>)[0].release.value)

        var claimsToAdd = {
            ...addItemStatement(siteEntities.properties.release_of, thisEntityPageData.title),
            ...addItemStatement(siteEntities.properties.instance_of, releaseTypeID)
        }

        await copyItem(thisEntityPageData, true, propertiesToKeep, claimsToAdd)
    })
}


var linkToID = (link) => link.replace(/.*\//, "")

async function chronologicalDataFeature(thisEntityPageData: WikibaseUIItem) {

    // Create chronological data storage element
    $(`#chronologicalDataLabel`).after(`<div id="mainChronologicalData" style="display: none"></div>`)

    // Prevent chronological data from running on releases whose artists are "various artists"
    if (entityHasStatement(
        siteEntities.properties.performer.id,
        [siteEntities.items.various_artists.id],
        thisEntityPageData.claims
    )) return;

    // Show loading label while data is retrieved
    $('#createRelease').after(`<div id="chronologicalDataLabel">Loading chronological data...</div>`)

    // Compile list of ids of the performers we need to query data for and initialize performer results objects in performerResults object
    for (let performer of thisEntityPageData.claims[siteEntities.properties.performer.id]) {
        const performerID = performer.mainsnak.datavalue.value.id

        const userLang = mw.config.get('wgContentLanguage')

        let queryVariables: QueryVariable[] = [
            parentVariable,
            new ItemQueryVariable('type', siteEntities.properties.instance_of, false),
            new DateQueryVariable('date', siteEntities.properties.publication_date, true),
            new MonolingualTextQueryVariable('title', siteEntities.properties.title, true),
            new ItemQueryVariable('language', siteEntities.properties.language_of_work_or_name, true)
        ]

        let query = `SELECT DISTINCT `

        for (let queryVariable of queryVariables) {
            query += queryVariable.createGroupQuery()
        }

        query += `WHERE {
        {
            SELECT `

        for (let queryVariable of queryVariables) {
            query += queryVariable.createSelectQuery()
        }

        query += `WHERE {
        SERVICE wikibase:label { bd:serviceParam wikibase:language "${userLang},mul". }
        VALUES ?performer {wd:${performerID}}
        ?release wdt:${siteEntities.properties.performer} ?performer.
        ?type wdt:${siteEntities.properties.subclass_of}* wd:${siteEntities.items.release_group}.
    `

        for (let queryVariable of queryVariables) {
            if (queryVariable instanceof PropertyQueryVariable) {
                if (queryVariable.optional) {
                    query += `OPTIONAL {
                    ${queryVariable.createQuery()}
                }
                `
                }
                else query += queryVariable.createQuery()
            }
        }

        query += `} 
        }
    } GROUP BY ${parentVariable.createSelectQuery()}`

        const chronologicalDataResponse = await sparqlQuery(query) as SelectResponse

        // Add performer heading
        const performerLabel = $(`#${siteEntities.properties.performer}`).find(`a[href='/wiki/${performerID}']`).html()
        $('#mainChronologicalData').append(`<h2>${performerLabel}</h2>`)

        // Add table
        const performerTableID = `${performerID}-chronological-data`
        const performerTableBodyID = `${performerID}-chronological-data-body`
        $('#mainChronologicalData').append(`<table id="${performerTableID}">
        <thead>
            <tr></tr>
        </thead>
        <tbody id="${performerTableBodyID}"></tbody>
    </table>`)

        // Add headings
        for (let queryVariable of queryVariables) {
            $(`#${performerTableID} thead tr`).append(`<th>${queryVariable.name}</th>`)
        }

        // Add rows
        for (var queryResult of (chronologicalDataResponse.results.bindings as Array<Results>)) {

            $(`#${performerTableBodyID}`).append(`<tr></tr>`)

            // Add cells
            for (let queryVariable of queryVariables) {
                const cell = queryVariable.resultToTableCell(queryResult)
                $(`#${performerTableBodyID} tr`).append(`<td>${cell}</td>`)
            }
        }

        $(`#${performerTableID}`).DataTable();

        // Add button to show data
        $(`#chronologicalDataLabel`).html(`<a id="mainChronologicalDataLink">Show chronological data</a>`)
        $('#mainChronologicalDataLink').on('click', function () {
            $('#mainChronologicalData').slideToggle('fast');
        });
    }
}

    