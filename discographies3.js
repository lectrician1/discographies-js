/**
 * THIS IS A COMPILED TYPESCRIPT FILE
 * SEE THE ACTUAL SOURCE CODE AT User:Lectrician1/discographies.js/ts.js
 *
 * Name: discographies.js
 * Description: Shows useful discography data and functions on
 *  discography items
 * Note: Only meant to work on page refresh. This is because the
 *  lag time between creating a statement like "publication date"
 *  and it showing up in BlazeGraph and then by this script is high.
 * Author: Lectrician1
 * License: CC0
 * Functions taken from:
 *  https://www.wikidata.org/wiki/User:Nikki/ExMusica.js
 *  https://www.wikidata.org/wiki/User:Magnus_Manske/duplicate_item.js
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
// Utility functions
var entityHTML = function (link, label) { return "<a href=\"".concat(link, "\">").concat(label, "</a>"); };
var addItemStatement = function (propID, valueID) {
    var _a;
    return (_a = {},
        _a[propID] = [
            {
                "mainsnak": {
                    "snaktype": "value",
                    "property": propID,
                    "datavalue": {
                        "value": {
                            "entity-type": "item",
                            "id": valueID
                        },
                        "type": "wikibase-entityid"
                    },
                    "datatype": "wikibase-item"
                },
                "type": "statement",
                "rank": "normal"
            }
        ],
        _a);
};
function entityHasStatement(property, values, entityClaims) {
    for (var _i = 0, _a = entityClaims[property]; _i < _a.length; _i++) {
        var claim = _a[_i];
        if (values.includes(claim.mainsnak.datavalue.value.id))
            return true;
    }
    return false;
}
function getEditToken(callback) {
    return __awaiter(this, void 0, void 0, function () {
        var d, token;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, $.get('/w/api.php', {
                        action: 'query',
                        meta: 'tokens',
                        format: 'json'
                    })];
                case 1:
                    d = _a.sent();
                    token = d.query.tokens.csrftoken;
                    if (typeof token == 'undefined') {
                        alert("Problem getting edit token");
                        return [2 /*return*/];
                    }
                    console.log(d);
                    return [4 /*yield*/, callback(token)];
                case 2:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    });
}
function createNewItem(q, data) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, getEditToken(function (token) {
                        return __awaiter(this, void 0, void 0, function () {
                            var d, nq, url;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        console.log(data);
                                        return [4 /*yield*/, $.post('/w/api.php', {
                                                action: 'wbeditentity',
                                                "new": 'item',
                                                data: JSON.stringify(data),
                                                token: token,
                                                summary: 'Item release created from ' + q,
                                                format: 'json'
                                            })];
                                    case 1:
                                        d = _a.sent();
                                        if (d.success == 1) {
                                            nq = d.entity.id;
                                            url = "/wiki/" + nq;
                                            window.open(url, '_blank');
                                        }
                                        else {
                                            console.log(d);
                                            alert("A problem occurred, check JavaScript console for errors");
                                        }
                                        return [2 /*return*/];
                                }
                            });
                        });
                    })];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    });
}
function copyItem(thisEntityPageData, askLabels, propertiesToKeep, claimsToAdd) {
    return __awaiter(this, void 0, void 0, function () {
        var d, eNow, data;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, $.get('/w/api.php', {
                        action: 'wbgetentities',
                        ids: thisEntityPageData.title,
                        format: 'json'
                    })];
                case 1:
                    d = _a.sent();
                    eNow = d.entities[thisEntityPageData.title];
                    $.each(eNow.claims, function (p, v) {
                        if (propertiesToKeep.includes(String(p)))
                            $.each(v, function (i, c) {
                                delete c.id;
                            });
                        else
                            delete eNow.claims[p];
                    });
                    data = {
                        // descriptions : e.descriptions || {} ,
                        // labels : e.labels || {} ,
                        // aliases : e.aliases || {} ,
                        labels: Object,
                        aliases: Object,
                        claims: __assign(__assign({}, eNow.claims), claimsToAdd)
                    };
                    console.log(data);
                    if (askLabels && window.confirm("Duplicate all labels and aliases? You will need to fix them in all languages!")) {
                        data.labels = eNow.labels || {};
                        data.aliases = eNow.aliases || {};
                    }
                    return [4 /*yield*/, createNewItem(thisEntityPageData.title, data)];
                case 2:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    });
}
function sparqlQuery(query) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, $.post(siteData.sparqlEndpoint, { query: query })];
                case 1: return [2 /*return*/, _a.sent()];
            }
        });
    });
}
function sparqlAsk(query, callback) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, sparqlQuery(query)];
                case 1:
                    (_a.sent()).boolean ? callback() : {};
                    return [2 /*return*/];
            }
        });
    });
}
var entityLinkNameIDHTML = function (name, id) { return "<a href=\"https://www.wikidata.org/wiki/Special:EntityData/".concat(id, "\">").concat(name, "</a>"); };
var WikibaseProperty = /** @class */ (function () {
    function WikibaseProperty(id, label) {
        this.id = id;
        this.label = label;
        this.id = id;
        this.label = label;
    }
    return WikibaseProperty;
}());
var WikibaseItemProperty = /** @class */ (function (_super) {
    __extends(WikibaseItemProperty, _super);
    function WikibaseItemProperty() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return WikibaseItemProperty;
}(WikibaseProperty));
var WikibaseDateProperty = /** @class */ (function (_super) {
    __extends(WikibaseDateProperty, _super);
    function WikibaseDateProperty() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return WikibaseDateProperty;
}(WikibaseProperty));
var WikibaseQuantityProperty = /** @class */ (function (_super) {
    __extends(WikibaseQuantityProperty, _super);
    function WikibaseQuantityProperty() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return WikibaseQuantityProperty;
}(WikibaseProperty));
var WikibaseMonolingualTextProperty = /** @class */ (function (_super) {
    __extends(WikibaseMonolingualTextProperty, _super);
    function WikibaseMonolingualTextProperty() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return WikibaseMonolingualTextProperty;
}(WikibaseProperty));
var QueryVariable = /** @class */ (function () {
    function QueryVariable(name) {
        this.name = name;
    }
    return QueryVariable;
}());
var PropertyQueryVariable = /** @class */ (function (_super) {
    __extends(PropertyQueryVariable, _super);
    function PropertyQueryVariable(name, property, optional) {
        var _this = _super.call(this, name) || this;
        _this.property = property;
        _this.optional = optional;
        return _this;
    }
    return PropertyQueryVariable;
}(QueryVariable));
var ResultQueryVariable = /** @class */ (function (_super) {
    __extends(ResultQueryVariable, _super);
    function ResultQueryVariable(name) {
        return _super.call(this, name) || this;
    }
    ResultQueryVariable.prototype.createGroupQuery = function () {
        return "?".concat(this.name, " ?").concat(this.name, "Label ");
    };
    ResultQueryVariable.prototype.createSelectQuery = function () {
        return "?".concat(this.name, " ?").concat(this.name, "Label ");
    };
    ResultQueryVariable.prototype.resultToTableCell = function (result) {
        return entityHTML(result[this.name], result["".concat(this.name, "Label")]);
    };
    return ResultQueryVariable;
}(QueryVariable));
var ItemQueryVariable = /** @class */ (function (_super) {
    __extends(ItemQueryVariable, _super);
    function ItemQueryVariable(name, property, optional) {
        return _super.call(this, name, property, optional) || this;
    }
    ItemQueryVariable.prototype.createGroupQuery = function () {
        return "(GROUP_CONCAT (DISTINCT ?".concat(this.name, "; SEPARATOR = \"|\") AS ?").concat(this.name, "s)\n(GROUP_CONCAT (DISTINCT ?").concat(this.name, "Label; SEPARATOR = \"|\") AS ?").concat(this.name, "Labels)\n");
    };
    ItemQueryVariable.prototype.createSelectQuery = function () {
        return "?".concat(this.name, " ?").concat(this.name, "Label ");
    };
    ItemQueryVariable.prototype.createQuery = function () {
        return "?".concat(parentVariable, " wdt:").concat(this.property.id, " ?").concat(this.name, ".\n");
    };
    ItemQueryVariable.prototype.resultToTableCell = function (result) {
        var links = result["".concat(this.name, "s")];
        var labels = result["".concat(this.name, "Labels")];
        if (links.value === '')
            return '';
        var splitLinks = links.value.split('|');
        var splitLabels = labels.value.split('|');
        return splitLinks.map(function (link, index) { return entityHTML(link, splitLabels[index]); }).join(', ');
    };
    return ItemQueryVariable;
}(PropertyQueryVariable));
var DateQueryVariable = /** @class */ (function (_super) {
    __extends(DateQueryVariable, _super);
    function DateQueryVariable(name, property, optional) {
        return _super.call(this, name, property, optional) || this;
    }
    DateQueryVariable.prototype.createGroupQuery = function () {
        return "(GROUP_CONCAT (DISTINCT ?".concat(this.name, "; SEPARATOR = \"|\") AS ?").concat(this.name, "s)\n");
    };
    DateQueryVariable.prototype.createSelectQuery = function () {
        return "?".concat(this.name, " ");
    };
    DateQueryVariable.prototype.createQuery = function () {
        return "?".concat(parentVariable, " wdt:").concat(this.property.id, " ?").concat(this.name, ".\n");
    };
    DateQueryVariable.prototype.resultToTableCell = function (result) {
        var dates = result["".concat(this.name, "s")];
        if (dates.value === '')
            return '';
        var splitDates = dates.value.split('|');
        var prettyDates = splitDates.map(function (date) { return new Date(date).toISOString().split('T')[0]; });
        return prettyDates.join(', ');
    };
    return DateQueryVariable;
}(PropertyQueryVariable));
var MonolingualTextQueryVariable = /** @class */ (function (_super) {
    __extends(MonolingualTextQueryVariable, _super);
    function MonolingualTextQueryVariable(name, property, optional) {
        return _super.call(this, name, property, optional) || this;
    }
    MonolingualTextQueryVariable.prototype.createGroupQuery = function () {
        return "(GROUP_CONCAT (DISTINCT ?".concat(this.name, "; SEPARATOR = \"|\") AS ?").concat(this.name, "s)\n(GROUP_CONCAT (DISTINCT ?").concat(this.name, "Language; SEPARATOR = \"|\") AS ?").concat(this.name, "Languages)\n");
    };
    MonolingualTextQueryVariable.prototype.createSelectQuery = function () {
        return "?".concat(this.name, " ?").concat(this.name, "Language ");
    };
    MonolingualTextQueryVariable.prototype.createQuery = function () {
        return "?".concat(parentVariable.name, " wdt:").concat(this.property.id, " ?").concat(this.name, ".\nBIND ( lang(?").concat(this.name, ") AS ?").concat(this.name, "Language )\n");
    };
    MonolingualTextQueryVariable.prototype.resultToTableCell = function (result) {
        var titles = result["".concat(this.name, "s")];
        var languages = result["".concat(this.name, "Languages")];
        if (titles.value === '')
            return '';
        var splitTitles = titles.value.split('|');
        var splitLanguages = languages ? languages.value.split('|') : [];
        var titlesWithLang = splitTitles.map(function (title, index) { return "".concat(title, " (").concat(splitLanguages[index], ")"); });
        return titlesWithLang.join(', ');
    };
    return MonolingualTextQueryVariable;
}(PropertyQueryVariable));
var parentVariable = new ResultQueryVariable('release');
var siteData;
switch (window.location.hostname) {
    case 'www.wikidata.org':
        siteData = {
            sparqlEndpoint: 'https://query.wikidata.org/sparql?format=json',
            entities: {
                items: {
                    release_group: {
                        id: 'Q108346082'
                    },
                    release: {
                        id: 'Q2031291'
                    },
                    various_artists: {
                        id: 'Q3108914'
                    },
                    musical_work_composition: {
                        id: 'Q105543609'
                    },
                    song: {
                        id: 'Q7366'
                    }
                },
                properties: {
                    instance_of: new WikibaseItemProperty('P31', 'instance_of'),
                    subclass_of: new WikibaseItemProperty('P279', 'subclass_of'),
                    title: new WikibaseMonolingualTextProperty('P1476', 'title'),
                    genre: new WikibaseItemProperty('P136', 'genre'),
                    performer: new WikibaseItemProperty('P175', 'performer'),
                    record_label: new WikibaseItemProperty('P264', 'record_label'),
                    publication_date: new WikibaseDateProperty('P577', 'publication_date'),
                    number_of_parts_of_this_work: new WikibaseQuantityProperty('P2635', 'number_of_parts_of_this_work'),
                    tracklist: new WikibaseItemProperty('P658', 'tracklist'),
                    release_of: new WikibaseItemProperty('P9831', 'release_of'),
                    form_of_creative_work: new WikibaseItemProperty('P7937', 'form_of_creative_work'),
                    language_of_work_or_name: new WikibaseItemProperty('P407', 'language_of_work_or_name')
                }
            }
        };
        break;
}
var siteEntities = siteData.entities;
var instanceOfClass = "wdt:".concat(siteEntities.properties.instance_of.id, "/wdt:").concat(siteEntities.properties.subclass_of.id, "*");
var api = new mw.Api;
mw.hook("wikibase.entityPage.entityLoaded").add(function (thisEntityPageData) {
    return __awaiter(this, void 0, void 0, function () {
        var ifReleaseGroupQuery;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    // Script run conditions
                    if (thisEntityPageData.type !== "item")
                        return [2 /*return*/];
                    if (!thisEntityPageData.claims.hasOwnProperty(siteEntities.properties.instance_of.id))
                        return [2 /*return*/];
                    ifReleaseGroupQuery = "ASK {\n        wd:".concat(thisEntityPageData.title, " ").concat(instanceOfClass, " wd:").concat(siteEntities.items.release_group, ";\n        wdt:").concat(siteEntities.properties.performer, " [].\n    }");
                    return [4 /*yield*/, sparqlQuery(ifReleaseGroupQuery)];
                case 1:
                    // Run 
                    (_a.sent()).boolean ? releaseGroupFeature(thisEntityPageData) : {};
                    return [2 /*return*/];
            }
        });
    });
});
function releaseGroupFeature(thisEntityPageData) {
    createReleaseFeature(thisEntityPageData);
    chronologicalDataFeature(thisEntityPageData);
}
function createReleaseFeature(thisEntityPageData) {
    $('body').append("<link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css\">\n  \n    <script type=\"text/javascript\" charset=\"utf8\" src=\"https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js\"></script>");
    // Create release button
    $('#toc').after("<div><a id=\"createRelease\">Create a release for this release group</a></div>");
    $('#createRelease').on('click', function () {
        return __awaiter(this, void 0, void 0, function () {
            var releaseGroupReleaseQuery, releaseGroupReleaseResponse, propertiesToKeep, releaseTypeID, claimsToAdd;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        releaseGroupReleaseQuery = "SELECT ?release WHERE {\n                    ?release wdt:".concat(siteEntities.properties.release_of, " wd:").concat(thisEntityPageData.claims.P31[0].mainsnak.datavalue.value.id, ";\n                             wdt:").concat(siteEntities.properties.subclass_of, "* wd:").concat(siteEntities.items.release, ".\n                  }");
                        return [4 /*yield*/, sparqlQuery(releaseGroupReleaseQuery)];
                    case 1:
                        releaseGroupReleaseResponse = _a.sent();
                        propertiesToKeep = [
                            siteEntities.properties.performer,
                            siteEntities.properties.genre,
                            siteEntities.properties.number_of_parts_of_this_work,
                            siteEntities.properties.publication_date,
                            siteEntities.properties.record_label,
                            siteEntities.properties.title,
                            siteEntities.properties.tracklist
                        ].map(function (property) { return property.id; });
                        releaseTypeID = linkToID(releaseGroupReleaseResponse.results.bindings[0].release.value);
                        claimsToAdd = __assign(__assign({}, addItemStatement(siteEntities.properties.release_of, thisEntityPageData.title)), addItemStatement(siteEntities.properties.instance_of, releaseTypeID));
                        return [4 /*yield*/, copyItem(thisEntityPageData, true, propertiesToKeep, claimsToAdd)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    });
}
var linkToID = function (link) { return link.replace(/.*\//, ""); };
function chronologicalDataFeature(thisEntityPageData) {
    return __awaiter(this, void 0, void 0, function () {
        var _i, _a, performer, performerID, userLang, queryVariables, query, _b, queryVariables_1, queryVariable, _c, queryVariables_2, queryVariable, _d, queryVariables_3, queryVariable, chronologicalDataResponse, performerLabel, performerTableID, performerTableBodyID, _e, queryVariables_4, queryVariable, _f, _g, queryResult, _h, queryVariables_5, queryVariable, cell;
        return __generator(this, function (_j) {
            switch (_j.label) {
                case 0:
                    // Create chronological data storage element
                    $("#chronologicalDataLabel").after("<div id=\"mainChronologicalData\" style=\"display: none\"></div>");
                    // Prevent chronological data from running on releases whose artists are "various artists"
                    if (entityHasStatement(siteEntities.properties.performer.id, [siteEntities.items.various_artists.id], thisEntityPageData.claims))
                        return [2 /*return*/];
                    // Show loading label while data is retrieved
                    $('#createRelease').after("<div id=\"chronologicalDataLabel\">Loading chronological data...</div>");
                    _i = 0, _a = thisEntityPageData.claims[siteEntities.properties.performer.id];
                    _j.label = 1;
                case 1:
                    if (!(_i < _a.length)) return [3 /*break*/, 4];
                    performer = _a[_i];
                    performerID = performer.mainsnak.datavalue.value.id;
                    userLang = mw.config.get('wgContentLanguage');
                    queryVariables = [
                        parentVariable,
                        new ItemQueryVariable('type', siteEntities.properties.instance_of, false),
                        new DateQueryVariable('date', siteEntities.properties.publication_date, true),
                        new MonolingualTextQueryVariable('title', siteEntities.properties.title, true),
                        new ItemQueryVariable('language', siteEntities.properties.language_of_work_or_name, true)
                    ];
                    query = "SELECT DISTINCT ";
                    for (_b = 0, queryVariables_1 = queryVariables; _b < queryVariables_1.length; _b++) {
                        queryVariable = queryVariables_1[_b];
                        query += queryVariable.createGroupQuery();
                    }
                    query += "WHERE {\n        {\n            SELECT ";
                    for (_c = 0, queryVariables_2 = queryVariables; _c < queryVariables_2.length; _c++) {
                        queryVariable = queryVariables_2[_c];
                        query += queryVariable.createSelectQuery();
                    }
                    query += "WHERE {\n        SERVICE wikibase:label { bd:serviceParam wikibase:language \"".concat(userLang, "\". }\n        VALUES ?performer {wd:").concat(performerID, "}\n        ?release wdt:").concat(siteEntities.properties.performer, " ?performer.\n        ?type wdt:").concat(siteEntities.properties.subclass_of, "* wd:").concat(siteEntities.items.release_group, ".\n    ");
                    for (_d = 0, queryVariables_3 = queryVariables; _d < queryVariables_3.length; _d++) {
                        queryVariable = queryVariables_3[_d];
                        if (queryVariable instanceof PropertyQueryVariable) {
                            if (queryVariable.optional) {
                                query += "OPTIONAL {\n                    ".concat(queryVariable.createQuery(), "\n                }\n                ");
                            }
                            else
                                query += queryVariable.createQuery();
                        }
                    }
                    query += "} \n        }\n    } GROUP BY ".concat(parentVariable.createSelectQuery());
                    return [4 /*yield*/, sparqlQuery(query)];
                case 2:
                    chronologicalDataResponse = _j.sent();
                    performerLabel = $("#".concat(siteEntities.properties.performer)).find("a[href='/wiki/".concat(performerID, "']")).html();
                    $('#mainChronologicalData').append("<h2>".concat(performerLabel, "</h2>"));
                    performerTableID = "".concat(performerID, "-chronological-data");
                    performerTableBodyID = "".concat(performerID, "-chronological-data-body");
                    $('#mainChronologicalData').append("<table id=\"".concat(performerTableID, "\">\n        <thead>\n            <tr></tr>\n        </thead>\n        <tbody id=\"").concat(performerTableBodyID, "\"></tbody>\n    </table>"));
                    // Add headings
                    for (_e = 0, queryVariables_4 = queryVariables; _e < queryVariables_4.length; _e++) {
                        queryVariable = queryVariables_4[_e];
                        $("#".concat(performerTableID, " thead tr")).append("<th>".concat(queryVariable.name, "</th>"));
                    }
                    // Add rows
                    for (_f = 0, _g = chronologicalDataResponse.results.bindings; _f < _g.length; _f++) {
                        queryResult = _g[_f];
                        $("#".concat(performerTableBodyID)).append("<tr></tr>");
                        // Add cells
                        for (_h = 0, queryVariables_5 = queryVariables; _h < queryVariables_5.length; _h++) {
                            queryVariable = queryVariables_5[_h];
                            cell = queryVariable.resultToTableCell(queryResult);
                            $("#".concat(performerTableBodyID, " tr")).append("<td>".concat(cell, "</td>"));
                        }
                    }
                    $("#".concat(performerTableID)).DataTable();
                    // Add button to show data
                    $("#chronologicalDataLabel").html("<a id=\"mainChronologicalDataLink\">Show chronological data</a>");
                    $('#mainChronologicalDataLink').on('click', function () {
                        $('#mainChronologicalData').slideToggle('fast');
                    });
                    _j.label = 3;
                case 3:
                    _i++;
                    return [3 /*break*/, 1];
                case 4: return [2 /*return*/];
            }
        });
    });
}
