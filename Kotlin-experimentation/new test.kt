class AskQuery(pattern: GroupGraphPattern) {
    val query: String = "ASK { \n${pattern.query} \n}"
}

class GroupGraphPattern(subjects: List<DirectStatement<*>>) {
    val query: String = subjects.map { subject ->

    }.joinToString("\n")
}

class Property<T>(private val id: String) : Predicate<T>() {
    val query: String = id
}

class DirectStatement<S : EntityDataType, O : DataType>(entity: Value<S>, predicate: Predicate<O>, value: Value<O>) {
    val query: String = "${entity.query} wdt:$predicate $value"
}

open class Value<T>(value: String) {
    val query: String = value
}

class QueryVariable<T>(private val name: String, private val selected: Boolean = false) : Value<T>("?${name}")

open class DataType

open class EntityDataType : DataType()

class ItemDataType : EntityDataType()

class PropertyDataType : EntityDataType()

class MonolingualText : DataType()

open class Predicate<T>

class SequencePath<T>(firstProperty: Property<T>, secondProperty: Property<*>, extraProperties: List<Property<*>>) : Predicate<T>() {
    val query: String = "wdt:$firstProperty/wdt:$secondProperty${extraProperties.joinToString("") { property -> "/wdt:$property" }}"
}

var instanceOf = QueryVariable<ItemDataType>("instanceOf")

fun main() {

    /*
    ASK {
        wd:${thisEntityPageData.title} ${instanceOfClass} wd:${siteEntities.items.release_group.id};
        wdt:${siteEntities.properties.performer.id} [].
    }
     */

    val hi = DirectStatement(Value<ItemDataType>("Q42"), Property<ItemDataType>("P31"), instanceOf)
}